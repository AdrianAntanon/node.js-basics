function hola(nombre) {
    return new Promise(function(resolve, reject){
        setTimeout(function () {
            console.log(`Hola ${nombre}`);
            resolve(nombre);
        }, 1000);
    });
}

function hablar(nombre) {
    return new Promise((resolve, reject) =>{
        setTimeout(function () {
            console.log('Bla bla bla...');
            resolve(nombre);
        }, 1000);
    })
}

function adios(nombre) {
    return new Promise( (resolve, reject) => {
        setTimeout(function () {
            console.log(`Adios ${nombre}`);
            // resolve();
            reject('ERROR 404')
        }, 1000);
    })
}


// Ejecución
console.log('Iniciando el proceso');
hola('Adri')
    .then(hablar)
    .then(hablar)
    .then(hablar)
    .then(adios) 
    .then((nombre) => {
        console.log('Terminado el proceso');
    })
    .catch(error => {
        console.log(`Error inesperado: ${error}`);
    })