const fs = require('fs');

function read(route, cb){
    fs.readFile(route, (err, data) => {
        // Leído
        // cb(data.toString());
        console.log(data.toString());
    })
}

read(__dirname + '/file.txt');

function write(route, content, cb){
    fs.writeFile(route, content, function(err){
        if(err) console.error('No he podido escribirlo', err);
        else console.log('Se ha escrito en el archivo');
    });
}

write(__dirname + '/archivo1.txt', 'Soy un archivo nuevo', console.log);

function deleteFile(route, cb){
    fs.unlink(route, cb);
}

deleteFile(__dirname + '/archivo1.txt', console.log);